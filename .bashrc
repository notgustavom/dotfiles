#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '
export EDITOR='/usr/bin/nano'
export BINBIN='/usr/sbin:/usr/local/sbin'
export PATH="$BINBIN:$HOME/.stew/bin:$HOME/.cargo/bin:$PATH"
export WLR_NO_HARDWARE_CURSORS=1
#export WLR_BACKENDS=wayland
export WLR_RENDERER=gles2
#export WLR_RENDERER_ALLOW_SOFTWARE=1
export MOZ_X11_EGL=1
export MOZ_ENABLE_WAYLAND=1
#export LIBVA_DRIVERS_PATH="/usr/lib/dri"
#export LIBVA_DRIVER_NAME=radeonsi
